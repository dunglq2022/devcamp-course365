const getAllCourseMiddile = (req, res, next) => {
    console.log('Get All Course Middile')
    next();
}

const getCourseByIdMiddile = (req, res, next) => {
    console.log('Get Course By Id Middile')
    next();
}

const createCourseByIdMiddle = (req, res, next) => {
    console.log('Create Course By Id Middile')
    next();
}

const updateCourseByIdMiddile = (req, res, next) => {
    console.log('Update Course By Id Middile')
    next();
}

const deleteCourseByIdMiddile = (req, res, next) => {
    console.log('Delete Course By Id Middile')
    next();
}

module.exports = {
    getAllCourseMiddile,
    getCourseByIdMiddile,
    createCourseByIdMiddle,
    updateCourseByIdMiddile,
    deleteCourseByIdMiddile
}