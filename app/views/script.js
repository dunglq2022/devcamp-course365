
"use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gURL_BASE = "/courses/"
  const gCONTENT_TYPE = "application/json;charset=UTF-8"
  var gCoursesDB = [];
  var gCoursesPopularDB = [];
  var gCoursesTrendingDB = [];
  var gCourersId = 0;
  var gCoursesObjTable = {};
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  $(document).ready(function() {
    $.ajax({
      type: "GET",
      url: gURL_BASE,
      dataType: "json",
      success: function(resposnText) {
        for(var bI = 0; bI < resposnText.length; bI ++){
        gCoursesDB.push(resposnText[bI]);
        }
        filterPopularCourses(gCoursesDB);
        filterTrendingCourses(gCoursesDB);
        hienThiFilterPopularToWebsite(gCoursesPopularDB);
        hienThiFilterTrendingToWebsite(gCoursesTrendingDB);
        }
    });
    //3 ---U----Update Course
    $("#course-list-table").on("click", "#update-icon", function(){
      onBtnUpdateCourseTable(this);
    });
    
   /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  

   /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //Hàm Filter vPopularCourse
  function filterPopularCourses(paramCousers) {
    "use strict";
      for (var bI = 0; bI < paramCousers.length; bI++) {
          if (paramCousers[bI].isPopular == true) {
              var vResultIsPopular = paramCousers[bI];
              console.log("Các Post Popular: " + JSON.stringify(vResultIsPopular, true, 2))
              gCoursesPopularDB.push(vResultIsPopular);
            }
        }
  }
  //Hàm hienThiFilterPopularToWebsite
  function hienThiFilterPopularToWebsite(paramObjPopular) {
    "use strict";
    for (var bI = 0; bI < 4; bI++) {  
      $("#id-most-popular").append(`<div class="col mb-4">
              <div class="card">
                <img src="` + paramObjPopular[bI].coverImage + `"class="card-img-top" alt="...">
                <div class="card-body">
                  <h6 class="card-title text-primary">` + paramObjPopular[bI].courseName + `</h6>
                  <p class="card-text"><i class="far fa-clock">&nbsp;</i>` + paramObjPopular[bI].duration + `&nbsp;`+ paramObjPopular[bI].level + `</p>
                  <div class="d-inline-flex">
                    <p class="font-card-weight ">` + paramObjPopular[bI].price + `$&nbsp;</p>
                    <p class="text-gach-line font-card-weight text-secondary">` + paramObjPopular[bI].discountPrice + `$</p>
                  </div>
                </div>
                <div class="card-footer ">
                  <img src="` + paramObjPopular[bI].teacherPhoto + `" alt="` + paramObjPopular[bI].teacherName + `" class="rounded-circle" height="30px"> 
                  <p class="d-inline-flex">&nbsp; ` + paramObjPopular[bI].teacherName + `</p>
                  <i class="far fa-bookmark" style="margin-left: 36px;"></i>                
                </div>
              </div>
            </div>`)
      }
  }
  //Hàm Filter vTrendingCourses
  function filterTrendingCourses(paramCousers) {
    "use strict";
      for (var bI = 0; bI < paramCousers.length; bI++){
          if (paramCousers[bI].isTrending == true){
              var vResultIsTrending = paramCousers[bI];
              //console.log("Các Post Trending: " + JSON.stringify(vResultIsTrending, true, 2))
              gCoursesTrendingDB.push(vResultIsTrending);
          }
      }              
  }
  //Hàm hienThiFilterTrendingToWebsite
  function hienThiFilterTrendingToWebsite(paramObjTrending) {
    "use strict";
    for (var bI = 0; bI < 4; bI++) {  
      $("#id-most-treding").append(`<div class="col mb-4">
              <div class="card">
                <img src="` + paramObjTrending[bI].coverImage + `"class="card-img-top" alt="...">
                <div class="card-body">
                  <h6 class="card-title text-primary">` + paramObjTrending[bI].courseName + `</h6>
                  <p class="card-text"><i class="far fa-clock">&nbsp;</i>` + paramObjTrending[bI].duration + `&nbsp;`+ paramObjTrending[bI].level + `</p>
                  <div class="d-inline-flex">
                    <p class="font-card-weight ">` + paramObjTrending[bI].price + `$&nbsp;</p>
                    <p class="text-gach-line font-card-weight text-secondary">` + paramObjTrending[bI].discountPrice + `$</p>
                  </div>
                </div>
                <div class="card-footer ">
                  <img src="` + paramObjTrending[bI].teacherPhoto + `" alt="` + paramObjTrending[bI].teacherName + `" class="rounded-circle" height="30px"> 
                  <p class="d-inline-flex">&nbsp; ` + paramObjTrending[bI].teacherName + `</p>
                  <i class="far fa-bookmark" style="margin-left: 36px;"></i>                
                </div>
              </div>
            </div>`)
    }
  }
});
