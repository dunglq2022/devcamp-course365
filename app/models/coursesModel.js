//Khai báo thư viện mongoose
const mongoose = require ('mongoose');
//Khai báo Schema
const Schema = mongoose.Schema;
//Tạo Schema
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    courseCode: {
        type: String,
        required: true,
        unique: true
    },
    courseName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    discountPrice: {
        type: Number,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    level: {
        type: String,
        require: true
    },
    coverImage: {
        type: String,
        required: true
    },
    teacherName: {
        type: String,
        required: true
    },
    teacherPhoto: {
        type: String,
        required: true
    },
    isPopular: {
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('course', courseSchema)