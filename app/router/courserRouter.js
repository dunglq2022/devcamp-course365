//Khai báo thư viện express 
const express =  require ('express');

//Khai báo router
const courseRouter = express.Router();

//Khai báo các middile
const {
    getAllCourseMiddile,
    getCourseByIdMiddile,
    createCourseByIdMiddle,
    updateCourseByIdMiddile,
    deleteCourseByIdMiddile
} = require ('../middilerware/courserMiddileware')

//Khai báo các controller
const {
    getAllCourseController,
    getCourseByIdController,
    createCourseByIdController,
    updateCourseByIdController,
    deleteCourseByIdController
} = require ('../controller/courseController')

courseRouter.get('/courses', getAllCourseMiddile, getAllCourseController)
courseRouter.get('/courses/:courseId', getCourseByIdMiddile, getCourseByIdController)
courseRouter.post('/courses', createCourseByIdMiddle, createCourseByIdController)
courseRouter.put('/courses/:courseId', updateCourseByIdMiddile, updateCourseByIdController)
courseRouter.delete('/courses/:courseId', deleteCourseByIdMiddile, deleteCourseByIdController)

module.exports = {courseRouter}