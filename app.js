//Khai báo thư viện express
const express = require ('express');

//Khai báo thư viện mongoose
const mongoose = require('mongoose')

//Khai báo port 
const port = 7000;

//Tạo app
const app = express();

//Khai báo express json body
app.use(express.json());

//Khai báo app.use view
app.use(express.static(__dirname + '/app/views'));

//Khai báo thư viện path view
const path = require ('path');

//Khai báo router
const {courseRouter} = require('./app/router/courserRouter')

//Connect MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365", (err) => {
    if(err) throw err;

    console.log("Connect MongoDB Successfully!")
})

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'app/views/index.html'));
})
app.get('/admin', (req, res) => {
    res.sendFile(path.join(__dirname, 'app/views/listCourse.html'));
})

app.use('/', courseRouter)

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
})