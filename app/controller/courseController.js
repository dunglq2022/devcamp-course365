//Khai báo thư viện mongoose
const mongoose = require ('mongoose');
//Khai báo model
const coursesModel = require('../models/coursesModel');
//Tạo các funtion CRUD
//Get All Courses
const getAllCourseController = (req, res) => {
    //B1 thu thập dữ liệu
    //B2 Validate
    //B3 Xử lý nghiệp vụ
    coursesModel.find((err, data) => {
        if (err) {
            res.status(500).json({
                message: `Error find All courses ${err}`
            })
        } else {
            res.status(200).json(data)
        }
    })
}

const getCourseByIdController = (req, res) => {
    //B1 thu thập dữ liệu
    let courseId = req.params.courseId
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(courseId)){
        res.status(400).json({
            message: 'Invalid courseId'
        })
    }
    //B3 Xử lý nghiệp vụ
    coursesModel.findById(courseId, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Error find course by id ${err}`
            })
        } else {
            res.status(200).json(data)
        }
    })
}

const createCourseByIdController = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    console.log(body);
    //B2 Validate
    if(!body.courseCode) {
        res.status(400).json({
            message: 'courseCode is valid'
        })
    }
    if(!body.courseName) {
        res.status(400).json({
            message: 'courseName is valid'
        })
    }
    if(!Number.isInteger(body.price) || body.price < 0) {
        res.status(400).json({
            message: 'price is valid'
        })
    }
    if(!Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
        res.status(400).json({
            message: 'discountPrice is valid'
        })
    }
    if(!body.duration) {
        res.status(400).json({
            message: 'duration is valid'
        })
    }
    if(!body.level) {
        res.status(400).json({
            message: 'level is valid'
        })
    }
    if(!body.coverImage) {
        res.status(400).json({
            message: 'coverImage is valid'
        })
    }
    if(!body.teacherName) {
        res.status(400).json({
            message: 'teacherName is valid'
        })
    }
    if(!body.teacherPhoto) {
        res.status(400).json({
            message: 'teacherPhoto is valid'
        })
    }
    //B3 Xử lý nghiệp vụ
    const newCourse = new coursesModel({
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
    })
    coursesModel.create(newCourse, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Course is not created: ${err}`
            })
        } else {
            res.status(201).json({
                message: 'Course is created',
                data: data
            })
        }
    })
}

const updateCourseByIdController = (req, res) => {
    let courseId = req.params.courseId
    let body = req.body
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(courseId)){
        res.status(400).json({
            message: 'Invalid courseId'
        })
    }

    //B3 Xử lý nghiệp vụ 
    const newCourse = new coursesModel({
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
    })
    coursesModel.findByIdAndUpdate(courseId, newCourse, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Course is not updated: ${err}`
            })
        } else {
            res.status(200).json({
                message: 'Course is updated',
                data: data
            })
        }
    })
}

const deleteCourseByIdController = (req, res) => {
    let courseId = req.params.courseId
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(courseId)){
        res.status(400).json({
            message: 'Invalid courseId'
        })
    }

    //B3 Xử lý nghiệp vụ  
    coursesModel.findByIdAndRemove(courseId, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Course is not deleted: ${err}`
            })
        } else {
            res.status(204).json({
                message: 'Course is deleted',
                data: data

            })
        }
    })
}

module.exports = {
    getAllCourseController,
    getCourseByIdController,
    createCourseByIdController,
    updateCourseByIdController,
    deleteCourseByIdController
}